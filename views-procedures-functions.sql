/*WIDOK: Widok 'lekcje' przedstawia najważniejsze informacje dotyczące przedmiotu w jednym miejscu 
(nazwa przedmiotu, profesor prowadzący, miejsce zajęć, potrzebny podręcznik). Tabela będąca wynikiem
bierze pod uwagę wszystkie przedmioty, nawet gdy nie mają przypisanej np. sali. W przykładzie użycia 
zademonstrowane jest korzystanie z widoku, aby uprościć zebranie informacji o ocenach ucznia.*/

CREATE VIEW lekcje AS
SELECT przedmioty.id_przedmiotu, nazwa, profesor.imie AS 'imie_profesora', 
profesor.nazwisko AS 'nazwisko_profesora', miejsca_zajec.pelna_nazwa AS 'miejsce_zajec', 
tytul AS 'podrecznik' FROM przedmioty
LEFT JOIN profesor ON przedmioty.id_profesora = profesor.id_profesora
LEFT JOIN miejsca_zajec ON przedmioty.id_miejsca = miejsca_zajec.id_miejsca
LEFT JOIN podrecznik ON przedmioty.id_przedmiotu = podrecznik.id_przedmiotu

--użycie - wyświetlenie przedmiotu/przedmiotów, z których wystawiono najwięcej ocen:

DECLARE @liczba_ocen int
SET @liczba_ocen = (SELECT TOP 1 COUNT(hogwart.ocena) AS 'ocena' FROM lekcje
LEFT JOIN hogwart ON lekcje.id_przedmiotu = hogwart.id_przedmiotu
GROUP BY lekcje.id_przedmiotu, nazwa
ORDER BY ocena DESC)

SELECT lekcje.id_przedmiotu, nazwa, COUNT(hogwart.ocena) AS 'liczba_ocen' FROM lekcje
LEFT JOIN hogwart ON lekcje.id_przedmiotu = hogwart.id_przedmiotu
GROUP BY lekcje.id_przedmiotu, nazwa
HAVING COUNT(hogwart.ocena) = @liczba_ocen
ORDER BY nazwa


/*PROCEDURA 'nowy_uczen': Procedura ta umożliwia dodanie do bazy danych nowego ucznia i jednocześnie
modyfikuje tabelę eliksiry_ucznia, przypisując nowo dodanemu uczniowi eliksir Felix Felicis.*/

CREATE PROCEDURE nowy_uczen 
	@id_dom int, @imie varchar(20), @nazwisko varchar(30), 
	@data_ur date, @rok int, @patronus varchar(30), @id_ucznia int
AS
INSERT INTO uczen(id_domu, imie, nazwisko, data_urodzenia, rok_nauki, patronus)
VALUES (@id_dom, @imie, @nazwisko, @data_ur, @rok, @patronus)
INSERT INTO eliksiry_ucznia(id_ucznia, id_eliksiru) VALUES (@id_ucznia, 2)

--użycie - dodanie nowego ucznia:

EXEC nowy_uczen 4, 'Jack', 'Sparrow', '2007-06-06', 5, 'sum', 11

SELECT * FROM uczen
SELECT * FROM eliksiry_ucznia


/*PROCEDURA 'przypisz_patronusa': Jeśli uczeń jest na 3 roku lub powyżej i nie ma patronusa, procedura 
przypisuje mu ten symbol. Jeśli nie ma takiego ucznia, procedura podwyższa wszystkim z 7 roku ocenę
z Obrony przed Czarną Magią (chyba że już ma najwyższą ocenę).*/

CREATE PROCEDURE przypisz_patronusa @patronus varchar(30)
AS
IF EXISTS (SELECT patronus FROM uczen WHERE patronus IS NULL AND rok_nauki >= 3)
	UPDATE uczen
    SET patronus = @patronus
    WHERE patronus IS NULL AND rok_nauki >= 3
ELSE
	UPDATE hogwart
    SET ocena = ocena + 1
    WHERE id_przedmiotu = 4 AND id_ucznia IN (SELECT id_ucznia FROM uczen WHERE patronus IS NOT NULL AND rok_nauki = 7)

--użycie - wprowadzenie do bazy ucznia bez patronusa i przypisanie mu go:

INSERT INTO uczen VALUES (2, 'Ygrek', 'Iksiński', '2006-10-13', 6, NULL)
SELECT*FROM uczen
SELECT*FROM hogwart

EXEC przypisz_patronusa @patronus = 'zet'
SELECT id_ucznia, imie, nazwisko, patronus FROM uczen
WHERE patronus IS NOT NULL

/*FUNKCJA 'adresy': Funkcja zbiera uczniów powyżej 3 roku, którzy nie mieszkają w Wielkiej
Brytanii. Nie przyjmuje parametrów. Zwraca tabelę z id, imieniem, nazwiskiem i krajem zamieszkania/
pochodzenia ucznia, a także oblicza i wyświetla ich wiek.*/

CREATE FUNCTION adresy()
RETURNS TABLE
AS
RETURN
(
	SELECT uczen.id_ucznia, imie, nazwisko, kraj, DATEDIFF(year, data_urodzenia, GETDATE()) AS 'wiek' 
  	FROM uczen
	JOIN adres_zamieszkania ON uczen.id_ucznia = adres_zamieszkania.id_ucznia
	WHERE kraj NOT LIKE 'Wielka Brytania'
)

--użycie - wyświetlenie funkcji z posortowaniem uczniów według wieku

SELECT * FROM dbo.adresy()
ORDER BY wiek


/*FUNKCJA 'studenci_i_eliksiry': Funkcja zwraca tabelę z id, imieniem, nazwiskiem i rokiem nauki
ucznia/uczniów z danego roku (parametr funkcji @rok), którzy posiadają największą liczbę eliksirów.*/

CREATE FUNCTION studenci_i_eliksiry(@rok int)
RETURNS TABLE
AS
RETURN
(
	SELECT uczen.id_ucznia, imie, nazwisko, rok_nauki, COUNT(eliksiry_ucznia.id_ucznia) AS 'liczba_eliksirow'
	FROM uczen
	LEFT JOIN eliksiry_ucznia ON uczen.id_ucznia = eliksiry_ucznia.id_ucznia
	WHERE rok_nauki = @rok
	GROUP BY uczen.id_ucznia, imie, nazwisko, rok_nauki
	HAVING COUNT(eliksiry_ucznia.id_ucznia) = (SELECT TOP 1 COUNT(eliksiry_ucznia.id_ucznia) AS 'liczba' FROM eliksiry_ucznia
                                               RIGHT JOIN uczen ON uczen.id_ucznia = eliksiry_ucznia.id_ucznia
                                               WHERE rok_nauki = @rok
                                               GROUP BY uczen.id_ucznia, imie, nazwisko, rok_nauki
                                               ORDER BY liczba DESC)
)

--użycie - uczniowie z 4 roku.

SELECT * FROM dbo.studenci_i_eliksiry(4)


/*WYZWALACZ 'absolwent': Przed wyzwalaczem stworzona zostaje tabela absolwenci. Gdy uczeń 
7 roku zostaje usunięty, automatycznie jego dane zapisują się w tej tabeli wraz z dodatkową 
kolumną data_zdania, która przyjmuje dzisiejszą datę.*/

CREATE TABLE absolwenci
(
  id_ucznia INTEGER NOT NULL PRIMARY KEY,
  id_domu INTEGER FOREIGN KEY REFERENCES dom(id_domu),
  imie VARCHAR(20) NOT NULL,
  nazwisko VARCHAR(30) NOT NULL,
  data_urodzenia DATE,
  rok_nauki INTEGER,
  patronus VARCHAR(40),
  data_zdania DATE
)

CREATE TRIGGER absolwent
ON uczen
AFTER DELETE
AS
BEGIN
	DECLARE kursor_deleted CURSOR
     FOR SELECT id_ucznia, id_domu, imie, nazwisko, data_urodzenia, rok_nauki, patronus
     FROM deleted
     OPEN kursor_deleted
     DECLARE
		@id_ucznia INT, @id_domu INT, @imie VARCHAR(20), @nazwisko VARCHAR(30),
		@data_urodzenia DATE, @rok_nauki INT, @patronus VARCHAR(40), @data_zdania DATE
	FETCH NEXT FROM kursor_deleted INTO
		@id_ucznia, @id_domu, @imie, @nazwisko, @data_urodzenia, @rok_nauki,
		@patronus
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF EXISTS (SELECT rok_nauki FROM uczen WHERE rok_nauki = 7)
        	INSERT INTO absolwenci 
            VALUES (@id_ucznia, @id_domu, @imie, @nazwisko, @data_urodzenia, @rok_nauki, @patronus, (GETDATE()))
		FETCH NEXT FROM kursor_deleted INTO
			@id_ucznia, @id_domu, @imie, @nazwisko, @data_urodzenia, @rok_nauki,
			@patronus
	END
	CLOSE kursor_deleted
	DEALLOCATE kursor_deleted
END
GO

--użycie - dodanie ucznia 7 roku (aby nie miał kluczy obcych), usunięcie go i sprawdzenie tabeli absolwenci:

INSERT INTO uczen VALUES (1, 'Hans', 'Gruber', '2005-08-31', 7, 'wąż rzeczny')
SELECT * FROM uczen

DELETE FROM uczen
WHERE id_ucznia = 12

SELECT * FROM absolwenci


/*WYZWALACZ 'oceny': Przyjmijmy, że ocena z przedmiotu może być obniżona tylko o jeden stopień.
Wyzwalacz będzie blokował obniżanie oceny o 2 stopnie lub więcej i wyświetlał komunikat błędu przy
próbie zmiany.*/

CREATE TRIGGER oceny
ON hogwart
INSTEAD OF UPDATE 
AS
BEGIN
	IF UPDATE(ocena)
    BEGIN 
    	DECLARE @zmieniona_ocena int 
        SELECT @zmieniona_ocena = ocena FROM inserted
        DECLARE @pierwsza_ocena int
        SELECT @pierwsza_ocena = ocena FROM deleted
        
        IF @zmieniona_ocena < (@pierwsza_ocena - 1)
        BEGIN
        	RAISERROR('Nie można obniżyć oceny', 16, 1)
            ROLLBACK
        END
	END
END
GO 

--użycie: próba obniżenia oceny z 6 na 4

UPDATE hogwart
SET ocena = 4
WHERE ocena = 6 AND id_ucznia = 3