# Hogwarts' database

This is a T-SQL project for Databases I subject developed in December 2022 and January 2023. It is entirely in Polish.

### Tech stack

`T-SQL` `MS SQL Server` `DB Designer 4`

### Description

The project represents a Hogwarts' database dedicated for students to manage their classes and grades. `.pdf` file contains description and visual representation of tables and their relations, while `.sql` files have the SQL code. 

- `values.sql` - creating tables and their sample values

- `queries.sql` - subqueries, aggregations and joins

- `views-procedures-functions.sql` - views, procedures, functions and triggers

### Run

To run this project you need MS SQL Server on your computer or to connect with it on [SQL Online IDE](https://sqliteonline.com/). You must run `values.sql` first, so every table will be available.
