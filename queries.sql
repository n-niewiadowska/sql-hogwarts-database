--Łączenie tabel: Oceny wszystkich uczniów.

SELECT uczen.id_ucznia, imie, nazwisko, przedmioty.nazwa AS 'przedmiot', ocena FROM uczen
LEFT JOIN hogwart ON hogwart.id_ucznia = uczen.id_ucznia
LEFT JOIN przedmioty ON hogwart.id_przedmiotu = przedmioty.id_przedmiotu
ORDER BY nazwisko, imie

--Łączenie tabel: Podręczniki z autorami potrzebne do przedmiotów (i do nich przypisane).

SELECT podrecznik.id_podrecznik, tytul, autor_podrecznika.imie AS 'imie_autora', 
autor_podrecznika.nazwisko AS 'nazwisko_autora', przedmioty.nazwa AS 'przedmiot' FROM podrecznik
INNER JOIN autor_podrecznika ON podrecznik.id_autora = autor_podrecznika.id_autora
INNER JOIN przedmioty ON podrecznik.id_przedmiotu = przedmioty.id_przedmiotu
ORDER BY nazwisko_autora, imie_autora

--Funkcje agregujące: Zliczanie posiadanych przez uczniów eliksirów w kolejności malejącej 
--(wliczając w to uczniów bez żadnego eliksiru). 

SELECT uczen.id_ucznia, imie, nazwisko, COUNT(eliksiry_ucznia.id_ucznia) AS 'posiadane_eliksiry'
FROM uczen
LEFT JOIN eliksiry_ucznia ON uczen.id_ucznia = eliksiry_ucznia.id_ucznia
GROUP BY uczen.id_ucznia, imie, nazwisko
ORDER BY posiadane_eliksiry DESC

--Funkcje agregujące: Znalezienie ucznia z najwyższą liczbą zebranych ocen.

SELECT uczen.id_ucznia, imie, nazwisko FROM uczen
LEFT JOIN hogwart ON uczen.id_ucznia = hogwart.id_ucznia
GROUP BY uczen.id_ucznia, imie, nazwisko
HAVING COUNT(hogwart.id_ucznia) = (SELECT TOP 1 COUNT(hogwart.id_ucznia) AS 'liczba_ocen' FROM hogwart
LEFT JOIN uczen ON hogwart.id_ucznia = uczen.id_ucznia
GROUP BY uczen.id_ucznia, imie, nazwisko
ORDER BY liczba_ocen DESC)
ORDER BY nazwisko, imie

--Funkcje agregujące i podzapytania: Znalezienie olimpiad, których cena jest powyżej średniej kosztów udziału. 

SELECT id_olimpiady, nazwa, koszt_udzialu FROM olimpiady
WHERE koszt_udzialu > (SELECT AVG(koszt_udzialu) FROM olimpiady)

--Podzapytania: Pokazanie sal, które są przypisane do zajęć obowiązkowych (encja przedmioty).

SELECT id_miejsca, pelna_nazwa FROM miejsca_zajec
WHERE id_miejsca IN (SELECT id_miejsca FROM przedmioty)

--Podzapytanie: Znalezienie autorów, którzy napisali najwięcej podręczników.

SELECT autor_podrecznika.id_autora, imie, nazwisko FROM autor_podrecznika
LEFT JOIN podrecznik ON autor_podrecznika.id_autora = podrecznik.id_autora
GROUP BY autor_podrecznika.id_autora, imie, nazwisko
HAVING COUNT(podrecznik.id_autora) = (SELECT TOP 1 COUNT(podrecznik.id_autora) AS 'liczba_ksiazek' FROM podrecznik
LEFT JOIN autor_podrecznika ON podrecznik.id_autora = autor_podrecznika.id_autora
GROUP BY autor_podrecznika.id_autora, imie, nazwisko
ORDER BY liczba_ksiazek DESC)
ORDER BY nazwisko, imie

--Podzapytanie: Wyświetlenie eliksirów, których nikt nie posiada. 

SELECT id_eliksiru, nazwa FROM eliksiry
WHERE id_eliksiru NOT IN (SELECT id_eliksiru FROM eliksiry_ucznia)
ORDER BY nazwa