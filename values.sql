SET DATEFORMAT ymd;

--CREATE DATABASE szkola_magii_hogwart
--USE szkola_magii_hogwart

CREATE TABLE profesor
(
  id_profesora INTEGER PRIMARY KEY IDENTITY,
  imie VARCHAR(20) NOT NULL,
  nazwisko VARCHAR(30) NOT NULL
)

CREATE TABLE dom
(
  id_domu INTEGER PRIMARY KEY IDENTITY,
  id_profesora INTEGER NOT NULL FOREIGN KEY REFERENCES profesor(id_profesora),
  nazwa VARCHAR(20) NOT NULL,
  dormitoria VARCHAR(40) NOT NULL
)

CREATE TABLE uczen
(
  id_ucznia INTEGER PRIMARY KEY IDENTITY,
  id_domu INTEGER NOT NULL FOREIGN KEY REFERENCES dom(id_domu),
  imie VARCHAR(20) NOT NULL,
  nazwisko VARCHAR(30) NOT NULL,
  data_urodzenia DATE NOT NULL CHECK (data_urodzenia > '2000-09-01'),
  rok_nauki INTEGER NOT NULL CHECK (rok_nauki <= 7),
  patronus VARCHAR(30) UNIQUE
)

CREATE TABLE adres_zamieszkania
(
  id_adresu INTEGER PRIMARY KEY IDENTITY,
  id_ucznia INTEGER NOT NULL UNIQUE FOREIGN KEY REFERENCES uczen(id_ucznia),
  kraj VARCHAR(30) DEFAULT 'Wielka Brytania',
  miasto VARCHAR(30) NOT NULL,
  ulica VARCHAR(50) NOT NULL,
  nr_domu INTEGER NOT NULL
)

CREATE TABLE eliksiry
(
  id_eliksiru INTEGER PRIMARY KEY IDENTITY,
  nazwa VARCHAR(20) NOT NULL UNIQUE,
  glowny_skladnik VARCHAR(30) NOT NULL,
  wlasciwosci VARCHAR(60) NOT NULL
)

CREATE TABLE eliksiry_ucznia
(
  id_el_ucznia INTEGER PRIMARY KEY IDENTITY,
  id_ucznia INTEGER NOT NULL FOREIGN KEY REFERENCES uczen(id_ucznia),
  id_eliksiru INTEGER NOT NULL FOREIGN KEY REFERENCES eliksiry(id_eliksiru)
)

CREATE TABLE miejsca_zajec
(
  id_miejsca INTEGER PRIMARY KEY IDENTITY,
  pelna_nazwa VARCHAR(30) NOT NULL
)

CREATE TABLE przedmioty
(
  id_przedmiotu INTEGER PRIMARY KEY IDENTITY,
  id_miejsca INTEGER NOT NULL FOREIGN KEY REFERENCES miejsca_zajec(id_miejsca),
  nazwa VARCHAR(50) NOT NULL UNIQUE,
  id_profesora INTEGER NOT NULL,
  data_egzaminu DATETIME NOT NULL
)

CREATE TABLE hogwart
(
  id_hogwart INTEGER PRIMARY KEY IDENTITY,
  id_ucznia INTEGER NOT NULL FOREIGN KEY REFERENCES uczen(id_ucznia),
  id_przedmiotu INTEGER NOT NULL FOREIGN KEY REFERENCES przedmioty(id_przedmiotu),
  id_profesora INTEGER NOT NULL FOREIGN KEY REFERENCES profesor(id_profesora),
  ocena INTEGER CHECK (ocena <= 6)
)

CREATE TABLE zajecia_dodatkowe
(
  id_zajecia INTEGER PRIMARY KEY IDENTITY,
  id_miejsca INTEGER NOT NULL FOREIGN KEY REFERENCES miejsca_zajec(id_miejsca),
  id_przedmiotu INTEGER NOT NULL FOREIGN KEY REFERENCES przedmioty(id_przedmiotu),
  nazwa VARCHAR(30) NOT NULL,
  czas_zajec TIME NOT NULL
)

CREATE TABLE olimpiady
(
  id_olimpiady INTEGER PRIMARY KEY IDENTITY,
  id_przedmiotu INTEGER NOT NULL FOREIGN KEY REFERENCES przedmioty(id_przedmiotu),
  nazwa VARCHAR(50) NOT NULL,
  data_olimpiady DATE UNIQUE,
  koszt_udzialu FLOAT CHECK (koszt_udzialu < 500.5)
)

CREATE TABLE autor_podrecznika
(
  id_autora INTEGER PRIMARY KEY IDENTITY,
  imie VARCHAR(20) NOT NULL,
  nazwisko VARCHAR(30)
)

CREATE TABLE podrecznik
(
  id_podrecznik INTEGER PRIMARY KEY IDENTITY,
  id_autora INTEGER NOT NULL FOREIGN KEY REFERENCES autor_podrecznika(id_autora),
  id_przedmiotu INTEGER NOT NULL FOREIGN KEY REFERENCES przedmioty(id_przedmiotu),
  tytul VARCHAR(50) NOT NULL UNIQUE
)


INSERT INTO profesor VALUES
  ('Minerwa', 'McGonagall'),
  ('Severus', 'Snape'),
  ('Gandalf', 'Siwobrody'),
  ('Rocky', 'Balboa'),
  ('Hoster', 'Hightower'),
  ('Wednesday', 'Addams'),
  ('Pomona', 'Sprout'),
  ('Filius', 'Flitwick'),
  ('Maciej', 'Jasnowidz');

INSERT INTO dom VALUES
  (1, 'Gryffindor', 'Wieża Gryffindora'),
  (8, 'Ravenclaw', 'Wieża zachodnia'),
  (7, 'Hufflepuff', 'Podziemia przy kuchni'),
  (2, 'Slytherin', 'Lochy'),
  (4, 'Alfenig', 'Wieża południowa');

INSERT INTO uczen VALUES
  (1, 'Harry', 'Potter', '2011-08-31', 1, 'rogacz'),
  (1, 'Hermiona', 'Granger', '2010-12-09', 1, 'wydra'),
  (2, 'Clarke', 'Griffin', '2008-04-20', 4, 'motyl'),
  (2, 'Jesse', 'Pinkman', '2006-05-05', 6, 'owczarek niemiecki'),
  (3, 'Cedrik', 'Diggory', '2005-11-01', 6, 'owca'),
  (3, 'Bella', 'Swan', '2009-03-30', 3, 'łabądź'),
  (4, 'Hannah', 'Kahnwald', '2007-01-16', 5, 'żmija'),
  (4, 'Tom', 'Riddle', '2004-12-24', 7, 'lis'),
  (5, 'Eragon', 'Cieniobójca', '2010-03-03', 2, 'smok'),
  (5, 'Waldemar', 'Kiepski', '2011-06-22', 1, 'york');

INSERT INTO adres_zamieszkania VALUES
  (10, 'Polska', 'Warszawa', 'Mickiewicza', 28),
  (7, 'Niemcy', 'Winden', 'Acornsweg', 6),
  (4, 'Stany Zjednoczone', 'Albuquerque', 'Michigan St', 77),
  (3, 'Kanada', 'Vancouver', 'Polis', 1),
  (1, 'Wielka Brytania', 'Londyn', 'Privet Drive', 4),
  (2, 'Wielka Brytania', 'Londyn', 'York Square', 25),
  (5, 'Irlandia', 'Dublin', 'Sheehan Ave', 15),
  (6, 'Wielka Brytania', 'Edynburg', 'Little Hope', 10),
  (9, 'Wielka Brytania', 'Carvahall', 'Palancar', 3),
  (8, 'Wielka Brytania', 'Londyn', 'Cave St', 55);

INSERT INTO eliksiry VALUES
  ('Amortencja', 'winogrono', 'Eliksir miłości'),
  ('Felix Felicis', 'żywica', 'Uszczęśliwia'),
  ('Eliksir spokoju', 'kamień księżycowy', 'Uspokaja'),
  ('Veritaserum', 'krew smoka', 'Zmusza do mówienia prawdy'),
  ('Eliksir Wielosokowy', 'włos', 'Zmienia w kogoś innego'),
  ('Wywar Żywej Śmierci', 'korzeń asfodelusa', 'Usypia'),
  ('Skele-gro', 'róg dwurożca', 'Odbudowuje złamane kości');

INSERT INTO eliksiry_ucznia VALUES
  (2, 5), (3, 3), (3, 5), (3, 4), (4, 1), (7, 2), (4, 2), (9, 3);

INSERT INTO miejsca_zajec VALUES
  ('Stadion Quidditcha'), ('Wieża Astronomiczna'), ('Szklarnia'), ('Szkolne błonia'), 
  ('Wschodnie skrzydło'), ('Pokój życzeń'), ('Sala do transmutacji'), ('Sala nr 5'), 
  ('Aula w lochach'), ('Wieża Nostradamusa'), ('Ogród wewnętrzny'), ('Przeklęte piętro');

INSERT INTO przedmioty VALUES
  (2, 'Astronomia', 5, '2023-06-23 12:00:00'),
  (9, 'Eliksiry', 2, '2023-03-15 08:30:00'),
  (8, 'Historia Magii', 3, '2023-04-28 10:00:00'),
  (6, 'Obrona przed Czarną Magią', 6, '2023-01-30 15:00:00'),
  (4, 'Opieka nad Magicznymi Stworzeniami', 3, '2023-05-04 13:45:00'),
  (5, 'Transmutacja', 1, '2023-06-20 09:15:00'),
  (1, 'Quidditch', 4, '2023-05-25 11:00:00'),
  (10, 'Wróżbiarstwo', 9, '2023-02-14 10:00:00'),
  (12, 'Zaklęcia', 8, '2023-04-03 08:00:00'),
  (3, 'Zielarstwo', 7, '2023-06-18 12:50:00');

INSERT INTO hogwart VALUES
  (3, 10, 7, 6),
  (3, 9, 8, 5),
  (3, 3, 3, 3),
  (4, 9, 8, 5),
  (4, 2, 2, 6),
  (5, 5, 3, 4),
  (10, 6, 1, 1),
  (8, 5, 6, 6),
  (8, 7, 4, 4);

INSERT INTO zajecia_dodatkowe VALUES
  (2, 1, 'Koło astronomiczne', '21:00:00'),
  (6, 9, 'Klub pojedynków', '16:30:00'),
  (11, 3, 'Stowarzyszenie historyków', '15:00:00'),
  (1, 7, 'Klub quidditcha', '13:00:00'),
  (4, 5, 'Miłośnicy zwierząt', '18:15:00');

INSERT INTO olimpiady VALUES
  (2, 'Egzotyczne wywary', '2023-01-15', 75.0),
  (6, 'Praktyczne zastosowanie transmutacji', '2023-02-28', 50.5),
  (3, 'Olimpiada wiedzy o powstaniach magicznych', '2022-12-31', 30.0),
  (10, 'Olimpiada wiedzy o roślinach', '2023-01-03', 55.0),
  (10, 'Opieka nad mandragorami', '2023-04-05', 100.5);

INSERT INTO autor_podrecznika VALUES
  ('Bathilda', 'Bagshot'),
  ('Newt', 'Scamander'),
  ('Nicolas', 'Flamel'),
  ('Robert', 'Lewandowski'),
  ('Jaskier', NULL),
  ('Mikołaj', 'Kopernik'),
  ('Sybilla', 'Fortivirtus'),
  ('Sauron', 'Mroczny');

INSERT INTO podrecznik VALUES
  (2, 5, 'Fantastyczne zwierzęta i jak je znaleźć'),
  (5, 3, 'Pół wieku poezji - historia magii'),
  (1, 4, 'Czarna magia i walka z nią'),
  (3, 2, 'Podstawy warzenia eliksirów'),
  (6, 1, 'O obrotach sfer niebieskich'),
  (4, 7, 'Najważniejsze ruchy w Quidditchu'),
  (2, 10, 'Rośliny i ich zastosowania'),
  (1, 9, 'Jak poprawnie rzucać zaklęcia?'),
  (7, 8, 'Tajniki wróżenia'),
  (8, 6, 'Podręcznik do transmutacji');